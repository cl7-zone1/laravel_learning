@extends('layouts.master')

@section('title', 'Show customer list Page')


@section('content')
    <h1>
        Show a customer
    </h1>
    <div>
        <label>customer name</label>
        <p>{{$customer->name}}</p>

        <label>customer email</label>
        <p>{{$customer->email}}</p>

        <label>customer address</label>
        <p>{{$customer->address}}</p>

        <label>customer phone</label>
        <p>{{$customer->phone}}</p>

        <label>customer tel</label>
        <p>{{$customer->tel}}</p>

       
    <br> <br><br> <br><br> <br>
@endsection