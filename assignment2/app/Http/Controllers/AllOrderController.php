<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Order;
use Illuminate\Http\Request;

class AllOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = [];
        $orders = Order::orderBy('id', 'desc')->paginate(11);
       
        $data["orders"] = $orders;

        return view('allOrder.index', $data);
    }
 


    
   
}
