<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreOrderDetailRequest;
use App\Http\Requests\UpdateOrderDetailRequest;
use App\Models\Order;
use App\Models\OrderDetail;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class OrderDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     */
 

    public function index()
    {
        $data = [];

        $orders = Order::orderBy('id', 'asc')->paginate(11);
        $OrderDetails = OrderDetail::whereIn('order_id', $orders->pluck('id'))
            ->orderBy('id', 'asc')->paginate(11);
        
        $data["orders"] = $orders;
        $data["OrderDetails"] = $OrderDetails;
        
        return view('OrderDetails.index', $data);
        
    }


    public function create()
    {
        $data = [];
        return view('OrderDetails.create',$data);
    }

   
    public function store(StoreOrderDetailRequest $request)
    {
        //
        $dataSave = [
            'order_id' => $request->order_id,
            'product_id' => $request->product_id,
            'quantity' => $request->quantity,
            'price' => $request->price,
        ];
         
        // //dd($fileUrl);
        try {
            //log ra khi create thành công
            OrderDetail::create($dataSave);
            Log::info('create a Post successfully!');
            //lưu vào db thành công
            return redirect()->route('OrderDetails.index')-> with('success', 'create successfully');
        } catch (Exception $exception) {
            //throw $th;
            //with là session flash
            //log ra lỗi
            Log::error($exception->getMessage());
            
            return redirect()->route('OrderDetails.index')-> with('error', 'create failed');
        }
    }

 
    public function show(int $id)
    {
        $data=[];
        $OrderDetail = OrderDetail::findOrFail($id);
        $data['OrderDetail'] = $OrderDetail;

        return view('OrderDetails.show', $data);
    }

 
    public function edit(int $id)
    {
        $data=[];
        $OrderDetail = OrderDetail::findOrFail($id);
        $data['OrderDetail'] = $OrderDetail;

        return view('OrderDetails.edit', $data);
    }


    public function update(UpdateOrderDetailRequest $request, int $id)
    {
        $OrderDetail = OrderDetail::findOrFail($id);

        $dataUpdate = [
            'order_id' => $request->order_id,
            'product_id' => $request->product_id,
            'quantity' => $request->quantity,
            'price' => $request->price,
           
        ];

        try {
            $OrderDetail->update($dataUpdate);
            Log::info('update  successfully!');
            return redirect()->route('OrderDetails.index')-> with('success', 'update successfully');
        
        } catch (Exception $exception) {

            Log::error($exception->getMessage());
            return redirect()->route('OrderDetails.index')-> with('error', 'update failed');
        }
    }

    public function destroy(int $id){

        $OrderDetail = OrderDetail::findOrFail($id);
        // dd($Post);
        try {
            //log ra khi update thành công
            Log::info('Delete successfully!');
            $OrderDetail->delete();
            //xóa record ra khỏi db thành công
            return redirect()->route('OrderDetails.index')-> with('success', 'delete successfully');
       } catch (Exception $exception) {
            //xóa record ra khỏi db thất bại
            Log::error($exception->getMessage());
            return redirect()->route('OrderDetails.index')-> with('error', 'delete failed');
       }
    }
}
