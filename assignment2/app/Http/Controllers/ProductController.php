<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Product;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = [];
        $products = Product::orderBy('id', 'desc');
       
        $products = $products->paginate(11);
        $data["products"] = $products;

        return view('products.index', $data);
    }

    public function create()
    {
        $data = [];
        return view('products.create',$data);
    }

   
    public function store(StoreProductRequest $request)
    {
        //
        $dataSave = [
            'name' => $request->name,
            'quantity' => $request->quantity,
            'price' => $request->price,

           
        ];
         
        // //dd($fileUrl);
        try {
            //log ra khi create thành công
            Product::create($dataSave);
            Log::info('create a Post successfully!');
            //lưu vào db thành công
            return redirect()->route('products.index')-> with('success', 'create successfully');
        } catch (Exception $exception) {
            //throw $th;
            //with là session flash
            //log ra lỗi
            Log::error($exception->getMessage());
            
            return redirect()->route('products.index')-> with('error', 'create failed');
        }
    }

 
    public function show(int $id)
    {
        $data=[];
        $product = Product::findOrFail($id);
        $data['product'] = $product;

        return view('products.show', $data);
    }

 
    public function edit(int $id)
    {
        $data=[];
        $product = Product::findOrFail($id);
        $data['product'] = $product;

        return view('products.edit', $data);
    }


    public function update(UpdateProductRequest $request, int $id)
    {
        $product = Product::findOrFail($id);

        $dataUpdate = [
            'name' => $request->name,
            'quantity' => $request->quantity,
            'price' => $request->price,
           
        ];

        try {
            $product->update($dataUpdate);
            Log::info('update  successfully!');
            return redirect()->route('products.index')-> with('success', 'update successfully');
        
        } catch (Exception $exception) {

            Log::error($exception->getMessage());
            return redirect()->route('products.index')-> with('error', 'update failed');
        }
    }

    public function destroy(int $id){

        $product = Product::findOrFail($id);
        // dd($Post);
        try {
            //log ra khi update thành công
            Log::info('Delete successfully!');
            $product->delete();
            //xóa record ra khỏi db thành công
            return redirect()->route('products.index')-> with('success', 'delete successfully');
       } catch (Exception $exception) {
            //xóa record ra khỏi db thất bại
            Log::error($exception->getMessage());
            return redirect()->route('products.index')-> with('error', 'delete failed');
       }
    }
}
