<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderByCustomerIDController extends Controller
{
    public function index()
    {
        $data = [];

        $customers = Customer::orderBy('id', 'asc')->paginate(11);
        $orders = Order::whereIn('customer_id', $customers->pluck('id'))
            ->orderBy('id', 'asc')->paginate(11);
        
        $data["customers"] = $customers;
        $data["orders"] = $orders;

        return view('OrderByCustomerID.index', $data);
    }
}
