<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;

class OrderByCurrentMonthController extends Controller
{
    public function index()
    {
        $data = [];
        $orders = Order::whereMonth('date', 7)->orderBy('id', 'desc')->paginate(11);
       
        $data["orders"] = $orders;

        return view('orders.index', $data);
    }
}
