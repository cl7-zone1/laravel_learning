<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreLoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function showForm(){
        return view('auth.login');
    }

    public function  handleForm(StoreLoginRequest $request){

         //dd($request->all());
         $credentials = $request->only('email', 'password');

         if(Auth::attempt($credentials)){
            return redirect()->intended('/home');
         }else{
            return redirect()->back()->with('error','invalid email or password');
         }

    }

    public function logout(){
        Auth::logout();
        return redirect('/login');

    }
}
