<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    


     public function index()
     {
         // lấy thông tin sản phẩm ở trong Cart (đang ở Session)
         $cart = session('cart_info');
         dd($cart);
     }
 
     public function store(Request $request)
     {
         // lấy thông tin sản phẩm ở trong Cart (đang ở Session)
         $cart = session('cart_info');
         
         // kiểm tra product đã có trong session chưa ?
         // nếu đã có: thì lấy quantity (đang có trong session) + 1
         // nếu chưa có: thì quantity = 1
         $productInCart = collect($cart)->firstWhere('product_id', $request->product_id);
         if (!empty($productInCart)) {
             $quantityInCart = $productInCart['quantity'];
             $productInfo = [
                 'quantity' => $quantityInCart + 1,
                 'product_id' => $request->product_id,
             ];
         } else {
             $productInfo = [
                 'quantity' => 1,
                 'product_id' => $request->product_id,
             ];
         }
 
         // dùng session để add thông tin sản phẩm vào Cart
         $cart[$request->product_id] = $productInfo;
 
         // Save session
         session(['cart_info' => $cart]);
 
         return redirect()->route('cart.index')
             ->with('success', 'Add product into cart successful.');
     }




    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

  
   

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
