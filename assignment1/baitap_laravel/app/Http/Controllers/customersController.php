<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCustomerRequest;
use App\Models\Customer;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class customersController extends Controller
{
  
    public function index()
    {
        $data = [];
        $customers = Customer::orderBy('id', 'desc');

        // Xử lý tìm kiếm
        if (request()->has('keyword')) {
            $keyword = '%' . request()->get('keyword') . '%';
            $customers->where(function($query) use ($keyword) {
                $query->where('name', 'LIKE', $keyword)
                      ->orWhere('email', 'LIKE', $keyword);
            });
        }

        if (request()->has('address')) {
            $address = request()->get('address');
            $customers->where('address', 'LIKE', '%' . $address . '%');
        }

        if (request()->has('tel')) {
            $tel = request()->get('tel');
            $customers->where('tel', 'LIKE', '%' . $tel . '%');
        }

        if (request()->has('from_age') && request()->has('to_age')) {
            $fromAge = request()->get('from_age');
            $toAge = request()->get('to_age');
            $currentYear = date('Y');
            $fromYear = $currentYear - $fromAge;
            $toYear = $currentYear - $toAge;
            $customers->whereBetween('birthday', [$toYear . '-01-01', $fromYear . '-12-31']);
        }

        if (request()->has('gender')) {
            $gender = request()->get('gender');
            $customers->where('gender', '=', $gender);
        }

        $genderOptions = Customer::distinct('gender')->pluck('gender');
        $data["genderOptions"] = $genderOptions;
        $customers = $customers->paginate(10);
        $data["customers"] = $customers;

        return view('customers.index', $data);
    }

    public function create()
    {
        $data = [];
        return view('customers.create',$data);
    }

   
    public function store(StoreCustomerRequest $request)
    {
        //
        $dataSave = [
            'name' => $request->name,
            'email' => $request->email,
            'address' => $request->address,
            'tel' => $request->tel,
            'birthday' => $request->birthday,
            'gender' => $request->gender,
            'country' => $request->country,
        ];
         
        // //dd($fileUrl);
        try {
            //log ra khi create thành công
            Customer::create($dataSave);
            Log::info('create a Post successfully!');
            //lưu vào db thành công
            return redirect()->route('customers.index')-> with('success', 'create successfully');
        } catch (Exception $exception) {
            //throw $th;
            //with là session flash
            //log ra lỗi
            Log::error($exception->getMessage());
            
            return redirect()->route('customers.index')-> with('error', 'create failed');
        }
    }

 
    public function show(int $id)
    {
        $data=[];
        $customer = Customer::findOrFail($id);
        $data['customer'] = $customer;

        return view('customers.show', $data);
    }

 
    public function edit(int $id)
    {
        $data=[];
        $customer = Customer::findOrFail($id);
        $data['customer'] = $customer;

        return view('customers.edit', $data);
    }


    public function update(Request $request, int $id)
    {
        $customer = Customer::findOrFail($id);

        $dataUpdate = [
            'name' => $request->name,
            'email' => $request->email,
            'address' => $request->address,
            'tel' => $request->tel,
            'birthday' => $request->birthday,
            'gender' => $request->gender,
            'country' => $request->country,
        ];

        try {
            $customer->update($dataUpdate);
            Log::info('update a Post successfully!');
            return redirect()->route('customers.index')-> with('success', 'update successfully');
        
        } catch (Exception $exception) {

            Log::error($exception->getMessage());
            return redirect()->route('customers.index')-> with('error', 'update failed');
        }
    }

    public function destroy(int $id){

        $customer = Customer::findOrFail($id);
        // dd($Post);
        try {
            //log ra khi update thành công
            Log::info('Delete customer successfully!');
            $customer->delete();
            //xóa record ra khỏi db thành công
            return redirect()->route('customers.index')-> with('success', 'delete successfully');
       } catch (Exception $exception) {
            //xóa record ra khỏi db thất bại
            Log::error($exception->getMessage());
            return redirect()->route('customers.index')-> with('error', 'delete failed');
       }
    }
}
