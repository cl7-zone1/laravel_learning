<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
   

    public function up(): void
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->string('name', 50)->change();
            $table->string('email', 50)->change();
            $table->string('address', 50)->change();
            $table->string('tel', 50)->change();
            $table->string('birthday', 50)->change();
            $table->string('gender', 50)->change();
            $table->string('country', 50)->change();
        });
    }

    public function down(): void
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->string('name', 50)->change();
            $table->string('email', 50)->change();
            $table->string('address', 50)->change();
            $table->string('tel', 50)->change();
            $table->string('birthday', 50)->change();
            $table->string('gender', 50)->change();
            $table->string('country', 50)->change();
        });
    }


};
