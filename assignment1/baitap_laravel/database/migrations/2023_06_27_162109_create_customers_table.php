<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name');
            $table->string('email');
            $table->string('address');
            $table->string('tel');
            $table->date('birthday');
            $table->tinyInteger('gender');
            $table->string('country');

        });
    }

 
    public function down(): void
    {
        Schema::dropIfExists('customers');
    }


};
