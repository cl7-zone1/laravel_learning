<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Customer>
 */
class CustomerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $gender = fake()->randomElement(['Male', 'Female']);
        return [
           
            'name' => fake()->name(),
            'email' => fake()->email(),
            'address' => 'da nang',
            'tel' => fake()->phoneNumber(),
            'birthday' => fake()->date(),
            'gender' => $gender,
            'country' => fake()->country(),

        ];
    }
}
