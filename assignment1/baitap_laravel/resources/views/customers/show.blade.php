@extends('layouts.master')

@section('title', 'Show customer list Page')


@section('content')
    <h1>
        Show a customer
    </h1>
    <div>
        <label>customer name</label>
        <p>{{$customer->name}}</p>

        <label>customer email</label>
        <p>{{$customer->email}}</p>

        <label>customer address</label>
        <p>{{$customer->address}}</p>

        <label>customer tel</label>
        <p>{{$customer->tel}}</p>

        <label>customer birthday</label>
        <p>{{$customer->birthday}}</p>

        <label>customer gender</label>
        <p>{{$customer->gender}}</p>

        <label>customer country</label>
        <p>{{$customer->country}}</p>
    </div>
    <br> <br><br> <br><br> <br>
@endsection