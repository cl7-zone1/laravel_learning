@extends('layouts.master')

@section('title', 'customer list Page')

@section('content')

    {{-- show message --}}
    @if(Session::has('success'))
        <p class="text-success">{{ Session::get('success') }}</p>
    @endif

    {{-- show error message --}}
    @if(Session::has('error'))
        <p class="text-danger">{{ Session::get('error') }}</p>
    @endif
    <a class="btn btn-primary" href="{{ route('customers.create') }}">Create customer</a>
    <br><br><br>

    @include('customers.search')

        @if(!empty($customers))
        <table>
           
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Address</th>
                <th>Tel</th>
                <th>Birthday</th>
                <th>Gender</th>
                <th>country</th>
                <th colspan="3">Action</th>
                @foreach($customers as $customer)
                <tr>
                    <td>{{ $customer->id }}</td>
                    <td>{{ $customer->name }}</td>
                    <td>{{ $customer->email }}</td>
                    <td>{{ $customer->address }}</td>
                    <td>{{ $customer->tel }}</td>
                    <td>{{ $customer->birthday }}</td>
                    <td>{{ $customer->gender }}</td>
                    <td>{{ $customer->country }}</td>
                    <td><a href="{{ route('customers.show', [ 'id' => $customer->id]) }}">Show</a></td>
                    <td><a href="{{ route('customers.edit', [ 'id' => $customer->id]) }}">Edit</a></td>
                    <td>
                        <form action="{{ route('customers.destroy', ['id' => $customer->id]) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-common" onclick="return confirm('Confirm delete ?')"><i class="fas fa-trash-alt"></i> Delete</button>
                        </form>
                    </td>
                   
                </tr>
            @endforeach
        </table>
        {{$customers->appends(request()->input())->links()}}

        @endif
@endsection


<!-- khai báo js chỉ dùng riêng cho trang này -->
@push('js')
    <script src="./js/task.js"></script>
@endpush
<!-- khai báo css chỉ dùng riêng cho trang này -->
@push('css')
    <link rel="stylesheet" href="/css/task.css" >
@endpush








