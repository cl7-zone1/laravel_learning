
<form method="get" action="">
    <h3>
        List users
    </h3>
    <div class="mb-2">
        <label>Name or email</label>
        <input name="keyword" value="{{ old('keyword', request()->get('keyword')) }}"
            class="form-control">
    </div>

    <div class="mb-2">
        <label>Address</label>
        <input name="address" value="{{ old('address', request()->get('address')) }}"
            class="form-control">
    </div>

    <div class="mb-2">
        <label>Tel</label>
        <input name="tel" value="{{ old('tel', request()->get('tel')) }}"
            class="form-control">
    </div>

    <div class="mb-2">
        <label>From age</label>
        <input name="from_age" value="{{ old('from_age', request()->get('from_age')) }}"
            class="form-control">
    </div>

    <div class="mb-2">
        <label>To age</label>
        <input name="to_age" value="{{ old('to_age', request()->get('to_age')) }}"
            class="form-control">
    </div>


    <div class="mb-2">
        <label>Gender</label>
        <select name="gender" class="form-control">
            <option></option>
            @foreach($genderOptions as $option)
                <option value="{{ $option }}" 
                    {{ old('gender', request()->get('gender')) == $option ? 'selected' : '' }}>
                    {{ $option }}
                </option>
            @endforeach
        </select>
    </div>

    <div class="mb-2">
        <button type="submit" class="btn btn-primary">Search</button>
    </div>
</form>
<br>


<br> <br>














