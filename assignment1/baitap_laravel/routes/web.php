<?php

use App\Http\Controllers\CardController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\customersController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RegisterController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('welcome');
})->name('home');

Route::get('/hello1', function () {
    var_dump('hello world1');
})->name('home1')-> middleware('test-middleware');

Route::get('/hello2', function () {
    var_dump('hello world2');
})->name('home2');


Route::get('/customers', [customersController::class, 'index'])-> name('customers.index');
Route::get('/customers/create', [customersController::class, 'create'])-> name('customers.create');
Route::post('/customers', [customersController::class, 'store'])-> name('customers.store');
Route::get('/customers/{id}', [customersController::class, 'show'])-> name('customers.show');
Route::get('/customers/{id}/edit', [customersController::class, 'edit'])-> name('customers.edit');
Route::put('/customers/{id}', [customersController::class, 'update'])-> name('customers.update');
Route::delete('/customers/{id}', [customersController::class, 'destroy'])-> name('customers.destroy');


Route::get('/login', [LoginController::class, 'showForm'])-> name('login');
Route::post('/login', [LoginController::class, 'handleForm'])-> name('handle-login');
//chức năng đăng ký 1 user mới
Route::get('/register', [RegisterController::class, 'showRegistertrationForm'])-> name('register');
Route::post('/register', [RegisterController::class, 'showRegistertrationForm'])-> name('handle-register');
//chức năng đăng xuất logout
Route::post('/logout', [LoginController::class, 'logout'])-> name('logout');


Route::get('/products', [ProductController::class, 'index'])-> name('products.index');
Route::get('/cart', [CartController::class, 'index'])-> name('cart.index');
Route::post('/cart', [CartController::class, 'store'])-> name('cart.store');

















