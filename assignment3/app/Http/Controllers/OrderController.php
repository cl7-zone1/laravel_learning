<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Models\Order;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     */

    public function index()
    {
        $data = [];
        $orders = Order::orderBy('id', 'desc')->paginate(11);

        $data["orders"] = $orders;

        return view('orders.index', $data);
    }
    

    
    public function create()
    {
        $data = [];
        return view('orders.create',$data);
    }

   
    public function store(StoreOrderRequest $request)
    {
        //
        $dataSave = [
            'customer_id' => $request->customer_id,
            'date' => $request->date,
           
        ];
         
        // //dd($fileUrl);
        try {
            //log ra khi create thành công
            Order::create($dataSave);
            Log::info('create a Post successfully!');
            //lưu vào db thành công
            return redirect()->route('orders.index')-> with('success', 'create successfully');
        } catch (Exception $exception) {
            //throw $th;
            //with là session flash
            //log ra lỗi
            Log::error($exception->getMessage());
            
            return redirect()->route('orders.index')-> with('error', 'create failed');
        }
    }

 
    public function show(int $id)
    {
        $data=[];
        $order = Order::findOrFail($id);
        $data['order'] = $order;

        return view('orders.show', $data);
    }

 
    public function edit(int $id)
    {
        $data=[];
        $order = Order::findOrFail($id);
        $data['order'] = $order;

        return view('orders.edit', $data);
    }


    public function update(UpdateOrderRequest $request, int $id)
    {
        $order = Order::findOrFail($id);

        $dataUpdate = [
            'customer_id' => $request->customer_id,
            'date' => $request->date,
           
        ];

        try {
            $order->update($dataUpdate);
            Log::info('update  successfully!');
            return redirect()->route('orders.index')-> with('success', 'update successfully');
        
        } catch (Exception $exception) {

            Log::error($exception->getMessage());
            return redirect()->route('orders.index')-> with('error', 'update failed');
        }
    }

    public function destroy(int $id){

        $order = Order::findOrFail($id);
        // dd($Post);
        try {
            //log ra khi update thành công
            Log::info('Delete successfully!');
            $order->delete();
            //xóa record ra khỏi db thành công
            return redirect()->route('orders.index')-> with('success', 'delete successfully');
       } catch (Exception $exception) {
            //xóa record ra khỏi db thất bại
            Log::error($exception->getMessage());
            return redirect()->route('orders.index')-> with('error', 'delete failed');
       }
    }
}
