<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\StoreProductRequest;
use App\Models\Product;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = [];
        // Tạo ra 1 biến
        $products = Product::orderBy('id', 'desc');

        return response()->json([
            'products' => $products
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreProductRequest $request)
    {
        $dataSave = [

            'name' => $request->name,
            'img' => $request->img,
            'quantity' => $request->quantity,
            'price' => $request->price,

        ];

        try {
            //log ra khi create thành công
            Log::info('create a Category successfully!');
            Product::create($dataSave);
            //lưu vào db thành công
            
            return response()->json(['success' => 'create successfully!'],201);
        } catch (Exception $exception) {
            //throw $th;
            //with là session flash
            //log ra lỗi
            Log::error($exception->getMessage());
            
            return response()->json(['error' => 'create failed'], 500);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
