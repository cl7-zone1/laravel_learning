<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreOrderDetailRequest;
use App\Http\Requests\UpdateOrderDetailRequest;
use App\Models\Order;
use App\Models\OrderDetail;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CheckoutViewController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
     {
         // lấy thông tin sản phẩm ở trong Cart (đang ở Session)
         $your_product = session('your_product');
        //dd($your_product);
         //$orders = Order::pluck('id','id');
         $orders = Order::orderBy('id', 'asc')->get();
         $data["orders"] =  $orders;
         $data["your_product"] = $your_product;

         return view('CheckoutView.index', $data);
     }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        
    }

    public function store(Request $request)
    {
        // lấy thông tin sản phẩm ở trong Cart (đang ở Session)
        $your_product = session('your_product');
        $yourproductInCart = collect($your_product)->firstWhere('product_id', $request->product_id);
        
        if (!empty($yourproductInCart)) {
            $yourproductInfo = [
                "name" =>  $request->name,
                'img' => $request->img,
                'quantity' => $request->quantity,
                'price' =>  $request->price,
                'product_id' => $request->product_id,
            ];
        } 
        else {
            $yourproductInfo = [
                "name" => $request->name,
                'img' => $request->img,
                'quantity' => $request->quantity,
                'price' => $request->price,
                'product_id' => $request->product_id,
            ];
        }

        $your_product = $yourproductInfo;
        // Save session
        session(['your_product' => $your_product]);

    
        return redirect()->route('CheckoutView.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
