<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreOrderDetailRequest;
use App\Http\Requests\UpdateOrderDetailRequest;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderDetail;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class OrderDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     */
 

     public function index()
     {
         $data = [];
         $OrderDetails = OrderDetail::orderBy('id', 'asc')->paginate(11);
 
         $data["OrderDetails"] = $OrderDetails;
 
         return view('OrderDetails.index', $data);
     }


    public function create()
    {
        $data = [];
        return view('OrderDetails.create',$data);
    }

   
    public function store(Request $request)
    {
        $carts = session('cart_info');
        $your_product = session('your_product');
        $current_date = date('Y-m-d');

    
        $dataCustomers = [
            'name' => $request->name,
            'email' => $request->email,
            'address' => $request->address,
            'phone' => $request->phone,
            "tel" => $your_product['name']
        ];
        
       
        foreach ($carts as $key => $cart) {
            if ($cart['product_id'] == $request->product_id) {
                unset($carts[$key]);
                break;
            }
        }

        if ($your_product['product_id'] == $request->product_id) {
            unset($your_product['product_id']);
            unset($your_product['name']);
            unset($your_product['img']);
            unset($your_product['quantity']);
            unset($your_product['price']);
        }
        
        session(['cart_info' => $carts]);
        session(['your_product' => $your_product]);
         
        // //dd($fileUrl);
        try {
            //log ra khi create thành công
            
            Customer::create($dataCustomers);

            $customer_id = Customer::max('id');
            //dd($customer_id+1);
            $dataOrders = [
                'customer_id' => $customer_id,
                'date' => $current_date
            ];
            Order::create($dataOrders);

            $order_id = Order::max('id');
            
            $dataSave = [
                'order_id' => $order_id,
                'product_id' => $request->product_id,
                'quantity' => $request->quantity,
                'price' => $request->price,
            ];
            //dd($dataSave);
            OrderDetail::create($dataSave);

            Log::info('create a Post successfully!');
            
            return redirect()->route('OrderDetails.index')-> with('success', 'create successfully');
        } catch (Exception $exception) {
            
            Log::error($exception->getMessage());
            
            return redirect()->route('OrderDetails.index')-> with('error', 'create failed');
        }
    }

 
    public function show(int $id)
    {
        $data=[];
        $OrderDetail = OrderDetail::findOrFail($id);
        $data['OrderDetail'] = $OrderDetail;

        return view('OrderDetails.show', $data);
    }

 
    public function edit(int $id)
    {
        $data=[];
        $OrderDetail = OrderDetail::findOrFail($id);
        $data['OrderDetail'] = $OrderDetail;

        return view('OrderDetails.edit', $data);
    }


    public function update(UpdateOrderDetailRequest $request, int $id)
    {
        $OrderDetail = OrderDetail::findOrFail($id);

        $dataUpdate = [
            'order_id' => $request->order_id,
            'product_id' => $request->product_id,
            'quantity' => $request->quantity,
            'price' => $request->price,
           
        ];

        try {
            $OrderDetail->update($dataUpdate);
            Log::info('update  successfully!');
            return redirect()->route('OrderDetails.index')-> with('success', 'update successfully');
        
        } catch (Exception $exception) {

            Log::error($exception->getMessage());
            return redirect()->route('OrderDetails.index')-> with('error', 'update failed');
        }
    }

    public function destroy(int $id){

        $OrderDetail = OrderDetail::findOrFail($id);
        // dd($Post);
        try {
            //log ra khi update thành công
            Log::info('Delete successfully!');
            $OrderDetail->delete();
            //xóa record ra khỏi db thành công
            return redirect()->route('OrderDetails.index')-> with('success', 'delete successfully');
       } catch (Exception $exception) {
            //xóa record ra khỏi db thất bại
            Log::error($exception->getMessage());
            return redirect()->route('OrderDetails.index')-> with('error', 'delete failed');
       }
    }
}
