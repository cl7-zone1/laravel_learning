<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CartController extends Controller
{
  
     public function index()
     {
         // lấy thông tin sản phẩm ở trong Cart (đang ở Session)
         $carts = session('cart_info');
         //dd($carts);
         $data["carts"] = $carts;
         return view('CartView.index', $data);
     }

    public function remove($product_id)
    {
        $carts = session('cart_info');
        foreach ($carts as $key => $cart) {
            if ($cart['product_id'] == $product_id) {
                unset($carts[$key]);
                break;
            }
        }

        session(['cart_info' => $carts]);

        return redirect()->back()->with('success', 'Product has been removed from cart.');
    }

     public function store(Request $request)
    {
        // lấy thông tin sản phẩm ở trong Cart (đang ở Session)
        $carts = session('cart_info');

        $productInCart = collect($carts)->firstWhere('product_id', $request->product_id);
        if (!empty($productInCart)) {
            $quantityInCart = $productInCart['quantity'];
            $productInfo = [
                "name" =>  $request->name,
                "img" =>  $request->img,
                'quantity' => $quantityInCart + 1,
                'price' =>  $request->price,
                'product_id' => $request->product_id,
            ];
        } 
        else {
            $productInfo = [
                "name" => $request->name,
                "img" =>  $request->img,
                'quantity' => 1,
                'price' => $request->price,
                'product_id' => $request->product_id,
            ];
        }

        // dùng session để add thông tin sản phẩm vào Cart
        $carts[$request->product_id] = $productInfo;

        // Save session
        session(['cart_info' => $carts]);

        return redirect()->route('CartView.index')
            ->with('success', 'Add product into cart successful.');
    }

    public function create()
    {
        //
    }

 
    public function show(string $id)
    {
        //
    }

 
    public function edit(string $id)
    {
        //
    }

   
    public function update(Request $request, string $id)
    {
        // lấy thông tin sản phẩm ở trong Cart (đang ở Session)
        $carts = session('cart_info');

        $productInCart = collect($carts)->firstWhere('product_id', $id);
        if (!empty($productInCart)) {
            $productInfo = [
                "name" =>  $request->name,
                "img" =>  $request->img,
                'quantity' => $request->quantity,
                'price' =>  $request->price,
                'product_id' => $id,
            ];
        } 
        // dùng session để add thông tin sản phẩm vào Cart
        $carts[$id] = $productInfo;

        // Save session
        session(['cart_info' => $carts]);

        return redirect()->route('CartView.index')
            ->with('success', 'Update cart successful.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
