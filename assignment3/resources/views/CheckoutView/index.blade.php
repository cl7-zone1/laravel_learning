@extends('layouts.master')

@section('title', 'product list Page')

@section('content')

    {{-- show message --}}
    @if(Session::has('success'))
        <p class="text-success">{{ Session::get('success') }}</p>
    @endif

    {{-- show error message --}}
    @if(Session::has('error'))
        <p class="text-danger">{{ Session::get('error') }}</p>
    @endif
    <br><br><br>
       <h3>
        <?php if(empty($your_product)){
            echo"Xin mời bạn thanh toán các đơn hàng còn lại!";
        }
        ?>
        </h3>
        @if(!empty($your_product))
            <table >
                <tbody>
                    <tr style="background-color: white;">
                        <td style="width:50%;">
                            <h3>Thông tin giỏ hàng
                            </h3>    
                        </td>
                        <td style="width:50%;">
                              
                        </td>   
                    </tr>
                </tbody>
            </table>
            <table >
                <tbody>
                    <tr style="background-color: white;">
                        <td style="width:50%;">
                        <form action="{{ route('OrderDetails.store') }}" method="post" >
                            @csrf
                           
                            <input readonly type="hidden" name='order_id' class="form-control" value="<?php echo rand(0,1000)?>">
                            <input type="hidden" name='product_id' class="form-control" value="<?php print_r($your_product['product_id']);?>">
                            <br>
                            <input type="hidden" name='quantity' class="form-control" value="<?php print_r($your_product['quantity']);?>">
                            <br>
                            <input type="hidden" name='price' class="form-control" value="<?php print_r($your_product['price']);?>">
                        
                            <input type="text" name='name' placeholder="name" class="form-control" value="">
                            <br>
                            <input type="text" name='email' placeholder="email" class="form-control" value="">
                            <br>
                            <input type="text" name='phone' placeholder="phone" class="form-control" value="">
                            <br>
                            <input type="text" name='address' placeholder="address" class="form-control" value="">

                            <br>
                            <div class="form-group">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td style="width:50%;">
                                                <a href="{{ route('CartView.index') }}">Giỏ hàng</a>
                                            </td>
                                            <td style="width:50%;">
                                                <button type="submit" class="btn btn-primary">Tiếp tục đến phương thức thanh toán</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </form>
                        </td>
                        <td style="width:50%;">
                            <p>{{ $your_product['product_id'] }}</p>
                            <img width="25%" src="{{ $your_product['img'] }}">
                            <h3>Diện thoại {{ $your_product['name'] }}</h3>
                            <div class="form-group">
                            <input type="text" name="free" placeholder="Mã giảm giá" class="mb-3" value="">
                            <button type="submit" class="btn btn-dark">Sử dụng</button>
                            </div>
                            <p>Phí vận chuyển</p>
                            <p>Tạm tính {{ $your_product['quantity'] * $your_product['price'] }}</p>
                            <h3>Tổng cộng {{ $your_product['quantity'] * $your_product['price'] }} VNĐ</h3>
                        </td>   
                    </tr>
                </tbody>
            </table>
            <br><br><br>
        @endif
@endsection

<!-- khai báo js chỉ dùng riêng cho trang này -->
@push('js')
    <script src="./js/task.js"></script>
@endpush
<!-- khai báo css chỉ dùng riêng cho trang này -->
@push('css')
    <link rel="stylesheet" href="/css/task.css" >
@endpush








