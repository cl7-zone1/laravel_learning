<!DOCTYPE html>
<html>
<head>
<title>@yield('title', 'Home page')</title>
<!-- file css -->
@include('layouts.css')
</head>
<body>
    @include('layouts.header')
    @include('layouts.menu')
    <main style="max-width:1150px;margin:0 auto; clear:both; display:flex;">
        <div class="container">
                @yield('content')
        </div>
    </main>
    @include('layouts.footer')
    @include('layouts.js')
</body>
</html>
















