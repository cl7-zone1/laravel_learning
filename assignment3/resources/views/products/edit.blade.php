@extends('layouts.master')

@section('title', 'Edit product list Page')


@section('content')
    <h1>
        Edit a product
    </h1>
    <form action="{{ route('products.update', ['id'=> $product->id]) }}" method="POST" enctype="multipart/form-data" >
        @csrf
        @method('PUT')
        <div class="form-group">
            <input type="text" name='name' placeholder="name" class="form-control" value="{{ old('name', $product->name) }}">
            @error('name')
                <div class="text-danger">{{$message}}</div>
            @enderror
        </div>


        <div class="form-group">
        <label>Image</label>

            <input type="file" name='img' placeholder="img" class="form-control" value="{{ old('img', $product->img) }}">
            @error('img')
                <div class="text-danger">{{$message}}</div>
            @enderror

            @if($product->img)
                <img width="20%" src="{{$product->img}}" alt="{{$product->img}}">
            @endif
        </div>


        <div class="form-group">
            <input type="text" name='quantity' placeholder="quantity" class="form-control" value="{{ old('quantity', $product->quantity) }}">
            @error('quantity')
                <div class="text-danger">{{$message}}</div>
            @enderror
        </div>

        <div class="form-group">
            <input type="text" name='price' placeholder="price" class="form-control" value="{{ old('price', $product->price) }}">
            @error('price')
                <div class="text-danger">{{$message}}</div>
            @enderror
        </div>



     
        
        <br>
        <div class="form-group">
            <a href="{{ route('products.index') }}" class="btn btn-secondary" >product list</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
    <br> <br><br> <br><br> <br>
@endsection