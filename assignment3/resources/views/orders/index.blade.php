@extends('layouts.master')

@section('title', 'order list Page')

@section('content')

    {{-- show message --}}
    @if(Session::has('success'))
        <p class="text-success">{{ Session::get('success') }}</p>
    @endif

    {{-- show error message --}}
    @if(Session::has('error'))
        <p class="text-danger">{{ Session::get('error') }}</p>
    @endif
    <a class="btn btn-primary" href="{{ route('orders.create') }}">Create order</a>
    <br><br><br>


        @if(!empty($orders))
        <table>
           
                <th>ID</th>
                <th>Customer ID</th>
                <th>Customer name</th>
                <th>Customer email</th>
                <th>Customer address</th>
                <th>Customer phone</th>
                <th>Customer tel</th>
                <th>Date</th>
               
                <th colspan="3">Action</th>
                @foreach($orders as $order)
                <tr>
                    @if ($order->customer)
                        <td>{{ $order->id }}</td>
                        <td>{{ $order->customer_id }}</td>
                        <td>{{ $order->customer->name }}</td>
                        <td>{{ $order->customer->email }}</td>
                        <td>{{ $order->customer->address }}</td>
                        <td>{{ $order->customer->phone }}</td>
                        <td>{{ $order->customer->tel }}</td>
                        <td>{{ $order->date }}</td>
                    @endif
                    <td><a href="{{ route('orders.show', [ 'id' => $order->id]) }}">Show</a></td>
                    <td><a href="{{ route('orders.edit', [ 'id' => $order->id]) }}">Edit</a></td>
                    <td>
                        <form action="{{ route('orders.destroy', ['id' => $order->id]) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-common" onclick="return confirm('Confirm delete ?')"><i class="fas fa-trash-alt"></i> Delete</button>
                        </form>
                    </td>
                   
                </tr>
            @endforeach
        </table>
        {{$orders->appends(request()->input())->links()}}

        @endif
@endsection


<!-- khai báo js chỉ dùng riêng cho trang này -->
@push('js')
    <script src="./js/task.js"></script>
@endpush
<!-- khai báo css chỉ dùng riêng cho trang này -->
@push('css')
    <link rel="stylesheet" href="/css/task.css" >
@endpush








