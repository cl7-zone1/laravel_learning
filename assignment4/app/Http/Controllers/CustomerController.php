<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCustomerRequest;
use App\Http\Requests\UpdateCustomersRequest;
use App\Models\Customer;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;



class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = [];
        $customers = Customer::orderBy('id', 'desc');
       
        $customers = $customers->paginate(11);
        $data["customers"] = $customers;

        return view('admin/customers.index', $data);
    }

    public function create()
    {
        $data = [];
        return view('admin/customers.create',$data);
    }

   
    public function store(StoreCustomerRequest $request)
    {
        //
        $dataSave = [
            'name' => $request->name,
            'email' => $request->email,
            'address' => $request->address,
            'phone' => $request->phone,
            'tel' => $request->tel,
        ];
         
        // //dd($fileUrl);
        try {
            //log ra khi create thành công
            Customer::create($dataSave);
            Log::info('create a Post successfully!');
            //lưu vào db thành công
            return redirect()->route('admin/customers.index')-> with('success', 'create successfully');
        } catch (Exception $exception) {
            //throw $th;
            //with là session flash
            //log ra lỗi
            Log::error($exception->getMessage());
            
            return redirect()->route('admin/customers.index')-> with('error', 'create failed');
        }
    }

 
    public function show(int $id)
    {
        $data=[];
        $customer = Customer::findOrFail($id);
        $data['customer'] = $customer;

        return view('admin/customers.show', $data);
    }

 
    public function edit(int $id)
    {
        $data=[];
        $customer = Customer::findOrFail($id);
        $data['customer'] = $customer;

        return view('admin/customers.edit', $data);
    }


    public function update(UpdateCustomersRequest $request, int $id)
    {
        $customer = Customer::findOrFail($id);

        $dataUpdate = [
            'name' => $request->name,
            'email' => $request->email,
            'address' => $request->address,
            'phone' => $request->phone,
            'tel' => $request->tel,
            'birthday' => $request->birthday,
            'gender' => $request->gender,
            'country' => $request->country,
        ];

        try {
            $customer->update($dataUpdate);
            Log::info('update a Post successfully!');
            return redirect()->route('admin/customers.index')-> with('success', 'update successfully');
        
        } catch (Exception $exception) {

            Log::error($exception->getMessage());
            return redirect()->route('admin/customers.index')-> with('error', 'update failed');
        }
    }

    public function destroy(int $id){

        $customer = Customer::findOrFail($id);
        // dd($Post);
        try {
            //log ra khi update thành công
            Log::info('Delete customer successfully!');
            $customer->delete();
            //xóa record ra khỏi db thành công
            return redirect()->route('admin/customers.index')-> with('success', 'delete successfully');
       } catch (Exception $exception) {
            //xóa record ra khỏi db thất bại
            Log::error($exception->getMessage());
            return redirect()->route('admin/customers.index')-> with('error', 'delete failed');
       }
    }
}
