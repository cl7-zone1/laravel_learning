<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Product;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        
        $data = [];
        $products = Product::orderBy('id', 'desc');
       
        $products = $products->paginate(11);
        $data["products"] = $products;

        // if(empty($_SESSION['login'])){
        //     return view('welcome');
        // }else{
        //     dd($_SESSION['login']);
        //     if($_SESSION['login']['email'] == "test@example.com"){

        //     }
        // }
        return view('users/ListProductView.index', $data);
    }

    public function create()
    {
        $data = [];
        
        return view('/admin/products.create',$data);
    }

    public function unsetSession()
    {
        session()->flush();
        
        return redirect()->back()->with('success', 'Unset session successfully!');
    }

   
    public function store(StoreProductRequest $request)
    {
        //
        $dataSave = [
            'name' => $request->name,
            'quantity' => $request->quantity,
            'price' => $request->price,

           
        ];

        $extension = $request->img->extension();
        $fileName = 'img_' . time() . '.' . $extension;
        // sử dụng hàm storeAs(): để lưu hình ảnh User lên vào folder 
        $path = $request->img->storeAs('imgs', $fileName, ['disk' => 'public']);
        //asset('/storage/app/public/imgs/'.$post->img)
        // hàm url(): dùng để lấy đường dẫn tới 1 file được lưu trữ trong Storage
        $fileUrl = Storage::url($path);
        //echo $fileUrl;
        // // set giá trị $fileUrl cho column img của table posts
        $dataSave['img'] = $fileUrl;
         
        // //dd($fileUrl);
        try {
            //log ra khi create thành công
            Product::create($dataSave);
            Log::info('create a Post successfully!');
            //lưu vào db thành công
            return redirect()->route('admin.home')-> with('success', 'create successfully');
        } catch (Exception $exception) {
            //throw $th;
            //with là session flash
            //log ra lỗi
            Log::error($exception->getMessage());
            
            return redirect()->route('admin.home')-> with('error', 'create failed');
        }
    }

 
    public function show(int $id)
    {
        $data=[];
        $product = Product::findOrFail($id);
        $data['product'] = $product;

        return view('/users/ListProductView.show', $data);
    }

 
    public function edit(int $id)
    {
        $data=[];
        $product = Product::findOrFail($id);
        $data['product'] = $product;

        return view('/admin/products.edit', $data);
    }


    public function update(UpdateProductRequest $request, int $id)
    {
        $product = Product::findOrFail($id);

        $current_img = $product->img;
        $current_img = str_replace('/storage', '', $current_img);

        $dataUpdate = [
            'name' => $request->name,
            'quantity' => $request->quantity,
            'price' => $request->price,
           
        ];

        if (!empty($request->img)) {
            // xử lý để upload file img lên server
            $extension = $request->img->extension();
            $fileName = 'img_' . time() . '.' . $extension;
            // sử dụng hàm storeAs(): để lưu hình ảnh User lên vào folder 
            $path = $request->img->storeAs('imgs', $fileName, ['disk' => 'public']);
            // hàm url(): dùng để lấy đường dẫn tới 1 file được lưu trữ trong Storage
            $fileUrl = Storage::url($path);
            //echo $fileUrl;
            // // set giá trị $fileUrl cho column img của table posts
            $dataUpdate['img'] = $fileUrl;  
        }

        try {
            $product->update($dataUpdate);
            Log::info('update  successfully!');
            return redirect()->route('admin.home')-> with('success', 'update successfully');
        
        } catch (Exception $exception) {

            Log::error($exception->getMessage());
            return redirect()->route('admin.home')-> with('error', 'update failed');
        }
    }

    public function destroy(int $id){

        $product = Product::findOrFail($id);
        // dd($Post);
        try {
            //log ra khi update thành công
            Log::info('Delete successfully!');
            $product->delete();
            //xóa record ra khỏi db thành công
            return redirect()->route('admin.home')-> with('success', 'delete successfully');
       } catch (Exception $exception) {
            //xóa record ra khỏi db thất bại
            Log::error($exception->getMessage());
            return redirect()->route('admin.home')-> with('error', 'delete failed');
       }
    }
}
