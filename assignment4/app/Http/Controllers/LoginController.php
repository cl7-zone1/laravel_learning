<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreLoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function showForm(){
        return view('public.login');
    }

    public function  handleForm(StoreLoginRequest $request){

         //dd($request->all());
         $credentials = $request->only('email', 'password');
         //dd($credentials);
         session_start();
         $_SESSION['login'] = $credentials;
         session(['login' => $credentials]);
         //dd(session('login'));
         //dd($_SESSION['login']);

         if($credentials['email'] == "test@example.com"){
            if(Auth::attempt($credentials)){
                return redirect()->intended('/admin');
             }else{
                return redirect()->back()->with('error','invalid email or password');
             }
         }else{
            if(Auth::attempt($credentials)){
                return redirect()->intended('/users');
            }else{
                return redirect()->back()->with('error','invalid email or password');
            }
         }

    }

    public function logout(){
        Auth::logout();
        return redirect('/login');

    }
}
