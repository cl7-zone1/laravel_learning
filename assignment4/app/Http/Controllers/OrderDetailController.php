<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreOrderDetailRequest;
use App\Http\Requests\UpdateOrderDetailRequest;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderDetail;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class OrderDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     */
 

     public function index()
     {
         $data = [];
         $OrderDetails = OrderDetail::orderBy('id', 'asc')->paginate(11);
 
         $data["OrderDetails"] = $OrderDetails;
 
         return view('admin/OrderDetails.index', $data);
     }


    public function create()
    {
        $data = [];
        return view('admin/OrderDetails.create',$data);
    }

   
    public function store(Request $request)
    {
        $carts = session('cart_info');
        $your_product = session('your_product');
        $current_date = date('Y-m-d');

        //lưu request từ form data vào mảng dataCustomers
        $dataCustomers = [
            'name' => $request->name,
            'email' => $request->email,
            'address' => $request->address,
            'phone' => $request->phone,
            "tel" => $your_product['name']
        ];
        
        //nếu product_id của cart đang thanh toán bằng  $request->product_id
        //thì sẽ xóa đi cart vừa thanh toán bên trang viewcart
        foreach ($carts as $key => $cart) {
            if ($cart['product_id'] == $request->product_id) {
                unset($carts[$key]);
                break;
            }
        }

        //xóa session hiển thị bên trang checkoutView
        if ($your_product['product_id'] == $request->product_id) {
            unset($your_product['product_id']);
            unset($your_product['name']);
            unset($your_product['img']);
            unset($your_product['quantity']);
            unset($your_product['price']);
        }
        //khởi động lại session sau khi xóa
        session(['cart_info' => $carts]);
        session(['your_product' => $your_product]);
         
        // //dd($fileUrl);
        try {
            //insert data từ dataCustomers vào customer DB
            Customer::create($dataCustomers);
            // lấy customer_id lớn nhất 
            // và thời gian hiện tại 
            // lưu vào mảng dataOrders
            $customer_id = Customer::max('id');
            $dataOrders = [
                'customer_id' => $customer_id,
                'date' => $current_date
            ];
            //insert data từ dataOrders vào customer DB
            Order::create($dataOrders);
            //lấy order_id lớn nhất 
            $order_id = Order::max('id');
            //lưu vào mảng dataSave
            $dataSave = [
                'order_id' => $order_id,
                'product_id' => $request->product_id,
                'quantity' => $request->quantity,
                'price' => $request->price,
            ];
            //insert data từ dataSave vào OrderDetail DB
            OrderDetail::create($dataSave);
            //log ra khi create thành công
            Log::info('create a Post successfully!');
            
            return redirect()->route('users/CheckoutView.index')-> with('success', 'Payment successful!');
        } catch (Exception $exception) {
            
            Log::error($exception->getMessage());
            
            return redirect()->route('users/CheckoutView.index')-> with('error', 'Payment failed!');
        }
    }

 
    public function show(int $id)
    {
        $data=[];
        $OrderDetail = OrderDetail::findOrFail($id);
        $data['OrderDetail'] = $OrderDetail;

        return view('admin/OrderDetails.show', $data);
    }

 
    public function edit(int $id)
    {
        $data=[];
        $OrderDetail = OrderDetail::findOrFail($id);
        $data['OrderDetail'] = $OrderDetail;

        return view('admin/OrderDetails.edit', $data);
    }


    public function update(UpdateOrderDetailRequest $request, int $id)
    {
        $OrderDetail = OrderDetail::findOrFail($id);

        $dataUpdate = [
            'order_id' => $request->order_id,
            'product_id' => $request->product_id,
            'quantity' => $request->quantity,
            'price' => $request->price,
           
        ];

        try {
            $OrderDetail->update($dataUpdate);
            Log::info('update  successfully!');
            return redirect()->route('admin.home')-> with('success', 'update successfully');
        
        } catch (Exception $exception) {

            Log::error($exception->getMessage());
            return redirect()->route('admin.home')-> with('error', 'update failed');
        }
    }

    public function destroy(int $id){

        $OrderDetail = OrderDetail::findOrFail($id);
        // dd($Post);
        try {
            //log ra khi update thành công
            Log::info('Delete successfully!');
            $OrderDetail->delete();
            //xóa record ra khỏi db thành công
            return redirect()->route('admin.home')-> with('success', 'delete successfully');
       } catch (Exception $exception) {
            //xóa record ra khỏi db thất bại
            Log::error($exception->getMessage());
            return redirect()->route('admin.home')-> with('error', 'delete failed');
       }
    }
}
