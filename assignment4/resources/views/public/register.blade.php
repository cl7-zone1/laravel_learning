@extends('layouts.master')

@section('title', 'OrderDetail list Page')

@section('content')

    {{-- show message --}}
    @if(Session::has('success'))
        <p class="text-success">{{ Session::get('success') }}</p>
    @endif

    {{-- show error message --}}
    @if(Session::has('error'))
        <p class="text-danger">{{ Session::get('error') }}</p>
    @endif
   
    <br><br><br>
    <h2>Register</h2>
    <form method="POST" action="">
            @csrf
            
            <label for="email">Email:</label>
            <input type="email" id="email" name="email" required><br><br>
            
            <label for="password">Password:</label>
            <input type="password" id="password" name="password" required><br><br>
            
            <input type="checkbox" id="remember" name="remember">
            <label for="remember">Remember Me</label><br><br>
            
            <input type="submit" value="Register">
        </form>


@endsection


<!-- khai báo js chỉ dùng riêng cho trang này -->
@push('js')
    <script src="./js/task.js"></script>
@endpush
<!-- khai báo css chỉ dùng riêng cho trang này -->
@push('css')
    <link rel="stylesheet" href="/css/task.css" >
@endpush








