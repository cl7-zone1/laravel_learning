@extends('layouts.master')

@section('title', 'Home page')

@section('content')
<br><br><br>
   <h1>Home page</h1>
   <br><br><br> 

   {{-- show message --}}
    @if(Session::has('success'))
        <p class="text-success">{{ Session::get('success') }}</p>
    @endif

    {{-- show error message --}}
    @if(Session::has('error'))
        <p class="text-danger">{{ Session::get('error') }}</p>
    @endif

    @if(!empty($products))
               
               @foreach($products as $product)
               <div class="card" style="width: 18rem; float:left; margin:20px;">
                   <p>{{ $product->id }}</p>
                   <img width="300px" height="350px" src="{{ $product->img }}" alt="image">
                   <div class="card-body">
                       <h5 class="card-title">{{ $product->name }}</h5>
                       <p class="card-text">{{ $product->price }}</p>
                       <div>
                            <a class="btn btn-info" aria-current="page" href="/login">Buy now</a>
                       </div>
                   </div>
               </div>
               
           @endforeach
           {{$products->appends(request()->input())->links()}}
           @endif


@endsection


<!-- khai báo js chỉ dùng riêng cho trang này -->
@push('js')
    <script src="./js/task.js"></script>
@endpush
<!-- khai báo css chỉ dùng riêng cho trang này -->
@push('css')
    <link rel="stylesheet" href="/css/task.css" >
@endpush








