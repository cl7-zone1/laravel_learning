@extends('admin.layouts.master')

@section('title', 'Edit customer list Page')


@section('content')
    <h1>
        Edit a customer
    </h1>
    <form action="{{ route('admin.customers.update', ['id'=> $customer->id]) }}" method="POST" >
        @csrf
        @method('PUT')
        <div class="form-group">
            <input type="text" name='name' placeholder="name" class="form-control" value="{{ old('name', $customer->name) }}">
            @error('name')
                <div class="text-danger">{{$message}}</div>
            @enderror
        </div>


        <div class="form-group">
            <input type="text" name='email' placeholder="email" class="form-control" value="{{ old('email', $customer->email) }}">
            @error('email')
                <div class="text-danger">{{$message}}</div>
            @enderror
        </div>

        <div class="form-group">
            <input type="text" name='address' placeholder="address" class="form-control" value="{{ old('address', $customer->address) }}">
            @error('address')
                <div class="text-danger">{{$message}}</div>
            @enderror
        </div>
        
        <div class="form-group">
            <input type="text" name='phone' placeholder="phone" class="form-control" value="{{ old('phone', $customer->phone) }}">
            @error('phone')
                <div class="text-danger">{{$message}}</div>
            @enderror
        </div>

        <div class="form-group">
            <input type="text" name='tel' placeholder="tel" class="form-control" value="{{ old('tel', $customer->tel) }}">
            @error('tel')
                <div class="text-danger">{{$message}}</div>
            @enderror
        </div>

        
        <br>
        <div class="form-group">
            <a href="{{ route('admin/customers.index') }}" class="btn btn-secondary" >Customer list</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
    <br> <br><br> <br><br> <br>
@endsection