@extends('admin.layouts.master')

@section('title', 'product list Page')

@section('content')

    {{-- show message --}}
    @if(Session::has('success'))
        <p class="text-success">{{ Session::get('success') }}</p>
    @endif

    {{-- show error message --}}
    @if(Session::has('error'))
        <p class="text-danger">{{ Session::get('error') }}</p>
    @endif
    <br><br><br>

        @if(!empty($products))
               
            @foreach($products as $product)

            <div class="card" style="width: 18rem; float:left; margin:20px;">
                <p>{{ $product->id }}</p>
                <img width="300px" height="350px" src="{{ $product->img }}" alt="image">
                <div class="card-body">
                    <h5 class="card-title">{{ $product->name }}</h5>
                    <p class="card-text">{{ $product->price }}</p>
                    <form action="{{ route('users/CartView.store') }}" method="post">
                        @csrf
                        <input type="hidden" name="name" value="{{ $product->name }}">
                        <input type="hidden" name="img" value="{{ $product->img }}">
                        <input type="hidden" name="quantity" value="1">
                        <input type="hidden" name="price" value="{{ $product->price }}">
                        <input type="hidden" name="product_id" value="{{ $product['id'] }}">
                        <button type="submit" class="btn btn-info">Add Cart</button>
                    </form>
                </div>
            </div>
            
        @endforeach
        {{$products->appends(request()->input())->links()}}
        @endif
@endsection


<!-- khai báo js chỉ dùng riêng cho trang này -->
@push('js')
    <script src="./js/task.js"></script>
@endpush
<!-- khai báo css chỉ dùng riêng cho trang này -->
@push('css')
    <link rel="stylesheet" href="/css/task.css" >
@endpush








