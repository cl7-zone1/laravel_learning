@extends('admin.layouts.master')

@section('title', 'Show product list Page')


@section('content')
    <h1>
        Show a product
    </h1>
    <div>
        <label>product name</label>
        <p>{{$product->name}}</p>

        <label>product quantity</label>
        <p>{{$product->quantity}}</p>

        <label>product price</label>
        <p>{{$product->price}}</p>
   
       
    <br> <br><br> <br><br> <br>
@endsection