@extends('admin.layouts.master')

@section('title', 'OrderDetail list Page')

@section('content')

    {{-- show message --}}
    @if(Session::has('success'))
        <p class="text-success">{{ Session::get('success') }}</p>
    @endif

    {{-- show error message --}}
    @if(Session::has('error'))
        <p class="text-danger">{{ Session::get('error') }}</p>
    @endif
    <a class="btn btn-primary" href="{{ route('admin/OrderDetails.create') }}">Create OrderDetail</a>
    <br><br><br>


        @if(!empty($OrderDetails))
        <table>
           
                <th>ID</th>
                <th>Order id</th>
                <th>Product id</th>
                <th>Quantity</th>
                <th>Price</th>
                

                <th colspan="3">Action</th>
                @foreach($OrderDetails as $OrderDetail)
                <tr>
                    <td>{{ $OrderDetail->id }}</td>
                    <td>{{ $OrderDetail->order_id }}</td>
                    <td>{{ $OrderDetail->product_id }}</td>
                    <td>{{ $OrderDetail->quantity }}</td>
                    <td>{{ $OrderDetail->price }}</td>
               

                    <td><a href="{{ route('admin/OrderDetails.show', [ 'id' => $OrderDetail->id]) }}">Show</a></td>
                    <td><a href="{{ route('admin/OrderDetails.edit', [ 'id' => $OrderDetail->id]) }}">Edit</a></td>
                    <td>
                        <form action="{{ route('admin.OrderDetails.destroy', ['id' => $OrderDetail->id]) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-common" onclick="return confirm('Confirm delete ?')"><i class="fas fa-trash-alt"></i> Delete</button>
                        </form>
                    </td>
                   
                </tr>
            @endforeach
        </table>
        {{$OrderDetails->appends(request()->input())->links()}}

        @endif
@endsection


<!-- khai báo js chỉ dùng riêng cho trang này -->
@push('js')
    <script src="./js/task.js"></script>
@endpush
<!-- khai báo css chỉ dùng riêng cho trang này -->
@push('css')
    <link rel="stylesheet" href="/css/task.css" >
@endpush








