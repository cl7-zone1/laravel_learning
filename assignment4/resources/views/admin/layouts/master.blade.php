<!DOCTYPE html>
<html>
<head>
<title>@yield('title', 'Home page')</title>
<!-- file css -->
@include('admin.layouts.css')
</head>
<body>
    @include('admin.layouts.header')
    @include('admin.layouts.menu')
    <main style="max-width:1150px;margin:0 auto; clear:both; display:flex;">
        <div class="container">
                @yield('content')
        </div>
    </main>
    @include('admin.layouts.footer')
    @include('admin.layouts.js')
</body>
</html>
















