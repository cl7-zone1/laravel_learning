{{-- https://getbootstrap.com/docs/5.2/getting-started/introduction/ --}}
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">

{{-- common --}}
<link rel="stylesheet" href="/css/common.css">

{{-- declare other file css --}}
@stack('css')