
<div class="container">
    <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container-fluid">
            <div class="collapse navbar-collapse" id="navbarScroll">
                <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="">User <?php session('login.email') ?> </a>
                    </li>        
                    <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="{{ route('users/ListProductView.index') }}">Products</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="{{ route('users.home') }}">home</a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="{{ route('users/CartView.index') }}">Carts</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="{{ route('users/CheckoutView.index') }}">CheckoutView</a>
                    </li>
                </ul>
                <form class="d-flex" role="search" action="{{ route('logout') }}" method="post">
                    @csrf
                    <button type="submit" class="btn btn-info">Logout</button>
                </form>
            </div>
        </div>
    </nav>
</div>
