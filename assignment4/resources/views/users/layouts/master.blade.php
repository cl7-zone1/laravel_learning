<!DOCTYPE html>
<html>
<head>
<title>@yield('title', 'Home page')</title>
<!-- file css -->
@include('users.layouts.css')
</head>
<body>
    @include('users.layouts.header')
    @include('users.layouts.menu')
    <main style="max-width:1150px;margin:0 auto; clear:both; display:flex;">
        <div class="container">
                @yield('content')
        </div>
    </main>
    @include('users.layouts.footer')
    @include('users.layouts.js')
</body>
</html>
















