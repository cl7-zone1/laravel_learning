@extends('layouts.master')

@section('title', 'Show product list Page')


@section('content')
    <h1>
       Cart
    </h1>
   
    <table>
           
           <th>Name</th>
           <th>Image</th>
           <th>Giá</th>
           <th>Số lượng</th>
           <th>Tổng Cộng</th>
          
           <tr>
               <td>{{ $product->name }}</td>
               <td><img width="100px" height="150px" src="{{ $product->img }}" alt="image"></td>
               <td>{{ $product->price }}</td>
               <td> <input readonly type="text" name='quantity' placeholder="quantity" class="form-control" value="{{ old('quantity', $product->quantity) }}">
                @error('quantity')
                    <div class="text-danger">{{$message}}</div>
                @enderror
                </td>
                <td>
                    {{ $product->price*$product->quantity }}
                </td>
              
           </tr>
      
   </table>
   
       
    <br> <br><br> <br><br> <br>
@endsection