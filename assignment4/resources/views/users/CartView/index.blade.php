@extends('users.layouts.master')

@section('title', 'cart list Page')

@section('content')

    {{-- show message --}}
    @if(Session::has('success'))
        <p class="text-success">{{ Session::get('success') }}</p>
    @endif

    {{-- show error message --}}
    @if(Session::has('error'))
        <p class="text-danger">{{ Session::get('error') }}</p>
    @endif
    <br><br><br>
       <h3>
        <?php if(empty($carts)){
            echo"Không có sản phẩm nào!";
        }?>
        </h3>
        @if(!empty($carts))
            <table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Tổng cộng</th>
                        <th colspan="2">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($carts as $cart)
                        <tr>
                            <td>{{ $cart['product_id'] }}</td>
                            <td>{{ $cart['name'] }}</td>
                            <td><img width="100px" height="150px" src="{{ $cart['img'] }}" alt="image"></td>
                            <td>{{ $cart['price'] }}</td>
                            <td>
   
                                <form action="{{ route('users/CartView.update', $cart['product_id'] ) }}" method="post">
                                    @csrf
                                    @method('PUT')
                                    <input type="hidden" name="product_id" value="{{ $cart['product_id'] }}">
                                    <input type="hidden" name="name" value="{{ $cart['name'] }}">
                                    <input type="hidden" name="img" value="{{ $cart['img'] }}">
                                    <input type="text" name='quantity' placeholder="quantity" class="form-control" value="{{ $cart['quantity'] }}">
                                    @error('quantity')
                                        <div class="text-danger">{{$message}}</div>
                                    @enderror
                                    <input type="hidden" name="price" value="{{ $cart['price'] }}">
                                    <br>
                                    <button type="submit" class="btn btn-dark">Cập nhật</button>
                                </form>
                            </td>
                            <td>
                                {{ $cart['quantity']*$cart['price'] }}
                            </td>
                            <td>
                                @if(!empty($cart['product_id']))
                                    <form action="{{ route('users/CartView.remove', $cart['product_id']) }}" method="post">
                                        @csrf
                                        <button type="submit" class="btn btn-danger btn-common" onclick="return confirm('Confirm delete ?')"><i class="fas fa-trash-alt"></i> Delete</button>
                                    </form>
                                @endif
                            </td>
                            <td>
                            <form action="{{ route('users/CheckoutView.store') }}" method="post">
                                @csrf
                                <input type="hidden" name="name" value="{{ $cart['name'] }}">
                                <input type="hidden" name="img" value="{{ $cart['img'] }}">
                                <input type="hidden" name="quantity" value="{{ $cart['quantity'] }}">
                                <input type="hidden" name="price" value="{{ $cart['price'] }}">
                                <input type="hidden" name="product_id" value="{{ $cart['product_id'] }}">
                                <button type="submit" class="btn btn-success">Thanh toán</button>
                            </form>
                            </td>
                           
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <br><br>
            <h3>Tổng cộng : <?php $sum = 0;
                foreach($carts as $cart){
                    $sum += $cart['quantity']*$cart['price'];
                }
                echo $sum;
                ?>
            </h3>
            <br>
          
            
        @endif
@endsection


<!-- khai báo js chỉ dùng riêng cho trang này -->
@push('js')
    <script src="./js/task.js"></script>
@endpush
<!-- khai báo css chỉ dùng riêng cho trang này -->
@push('css')
    <link rel="stylesheet" href="/css/task.css" >
@endpush








