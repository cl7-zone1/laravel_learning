@extends('users.layouts.master')

@section('title', 'Create OrderDetail list Page')


@section('content')
    <h1>
        Create a OrderDetail
    </h1>
    <form action="{{ route('CheckoutView.store') }}" method="POST" >
        @csrf

        <div class="form-group">
            <input type="text" name='order_id' placeholder="order_id" class="form-control" value="">
            @error('order_id')
                <div class="text-danger">{{$message}}</div>
            @enderror
        </div>

        <div class="form-group">
            <input type="text" name='product_id' placeholder="product_id" class="form-control" value="">
            @error('product_id')
                <div class="text-danger">{{$message}}</div>
            @enderror
        </div>
       

        <div class="form-group">
            <input type="text" name='quantity' placeholder="quantity" class="form-control" value="">
            @error('quantity')
                <div class="text-danger">{{$message}}</div>
            @enderror
        </div>
       
        <div class="form-group">
            <input type="text" name='price' placeholder="price" class="form-control" value="">
            @error('price')
                <div class="text-danger">{{$message}}</div>
            @enderror
        </div>


        <br>
        <div class="form-group">
            <a href="{{ route('CheckoutView.index') }}" class="btn btn-secondary" >OrderDetails list</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
    <br> <br><br> <br><br> <br>
@endsection














