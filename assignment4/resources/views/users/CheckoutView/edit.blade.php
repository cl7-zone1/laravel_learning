@extends('layouts.master')

@section('title', 'Edit OrderDetail list Page')


@section('content')
    <h1>
        Edit a OrderDetail
    </h1>
    <form action="{{ route('CheckoutView.update', ['id'=> $OrderDetail->id]) }}" method="POST" >
        @csrf
        @method('PUT')


        <div class="form-group">
            <input type="text" name='order_id' placeholder="order_id" class="form-control" value="{{ old('order_id', $OrderDetail->order_id) }}">
            @error('order_id')
                <div class="text-danger">{{$message}}</div>
            @enderror
        </div>

        <div class="form-group">
            <input type="text" name='product_id' placeholder="product_id" class="form-control" value="{{ old('product_id', $OrderDetail->product_id) }}">
            @error('product_id')
                <div class="text-danger">{{$message}}</div>
            @enderror
        </div>



        <div class="form-group">
            <input type="text" name='quantity' placeholder="quantity" class="form-control" value="{{ old('quantity', $OrderDetail->quantity) }}">
            @error('quantity')
                <div class="text-danger">{{$message}}</div>
            @enderror
        </div>

        <div class="form-group">
            <input type="text" name='price' placeholder="price" class="form-control" value="{{ old('price', $OrderDetail->price) }}">
            @error('price')
                <div class="text-danger">{{$message}}</div>
            @enderror
        </div>
      
        
        <br>
        <div class="form-group">
            <a href="{{ route('CheckoutView.index') }}" class="btn btn-secondary" >OrderDetail list</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
    <br> <br><br> <br><br> <br>
@endsection