<?php

use App\Http\Controllers\adminController;
use App\Http\Controllers\AllOrderController;
use App\Http\Controllers\AllOrderDetailController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CheckoutViewController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\OrderByCurrentMonthController;
use App\Http\Controllers\OrderByCustomerIDController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\OrderDetailController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\publicController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\usersController;
use App\Models\Product;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {


    $data = [];
    $products = Product::orderBy('id', 'desc');
   
    $products = $products->paginate(11);
    $data["products"] = $products;

    return view('welcome', $data);
});

Route::get('admin/customers', [CustomerController::class, 'index'])-> name('admin/customers.index');
Route::get('admin/customers/create', [CustomerController::class, 'create'])-> name('admin/customers.create');
Route::post('admin/customers', [CustomerController::class, 'store'])-> name('admin.customers.store');
Route::get('admin/customers/{id}', [CustomerController::class, 'show'])-> name('admin.customers.show');
Route::get('admin/customers/{id}/edit', [CustomerController::class, 'edit'])-> name('admin.customers.edit');
Route::put('admin/customers/{id}', [CustomerController::class, 'update'])-> name('admin.customers.update');
Route::delete('admin/customers/{id}', [CustomerController::class, 'destroy'])-> name('admin.customers.destroy');

Route::get('admin/orders', [OrderController::class, 'index'])-> name('admin/orders.index');
Route::get('admin/orders/create', [OrderController::class, 'create'])-> name('admin/orders.create');
Route::post('admin/orders', [OrderController::class, 'store'])-> name('admin.orders.store');
Route::get('admin/orders/{id}', [OrderController::class, 'show'])-> name('admin/orders.show');
Route::get('admin/orders/{id}/edit', [OrderController::class, 'edit'])-> name('admin/orders.edit');
Route::put('admin/orders/{id}', [OrderController::class, 'update'])-> name('admin.orders.update');
Route::delete('admin/orders/{id}', [OrderController::class, 'destroy'])-> name('admin.orders.destroy');

Route::get('users/ListProductView', [ProductController::class, 'index'])-> name('users/ListProductView.index');
Route::get('admin/ListProductView', [ProductController::class, 'index'])-> name('admin/ListProductView.index');

Route::get('/products', [ProductController::class, 'index'])-> name('products.index');
Route::get('admin/products/create', [ProductController::class, 'create'])-> name('admin/products.create');
Route::post('admin/products', [ProductController::class, 'store'])-> name('admin.products.store');
Route::get('admin/products/{id}', [ProductController::class, 'show'])-> name('admin/products.show');
Route::get('admin/products/{id}/edit', [ProductController::class, 'edit'])-> name('admin/products.edit');
Route::put('admin/products/{id}', [ProductController::class, 'update'])-> name('admin.products.update');
Route::delete('admin/products/{id}', [ProductController::class, 'destroy'])-> name('admin.products.destroy');

Route::get('admin/OrderDetails', [OrderDetailController::class, 'index'])-> name('admin/OrderDetails.index');
Route::get('admin/OrderDetails/create', [OrderDetailController::class, 'create'])-> name('admin/OrderDetails.create');
Route::post('admin/OrderDetails', [OrderDetailController::class, 'store'])-> name('admin.OrderDetails.store');
Route::get('admin/OrderDetails/{id}', [OrderDetailController::class, 'show'])-> name('admin/OrderDetails.show');
Route::get('admin/OrderDetails/{id}/edit', [OrderDetailController::class, 'edit'])-> name('admin/OrderDetails.edit');
Route::put('admin/OrderDetails/{id}', [OrderDetailController::class, 'update'])-> name('admin.OrderDetails.update');
Route::delete('admin/OrderDetails/{id}', [OrderDetailController::class, 'destroy'])-> name('admin.OrderDetails.destroy');

Route::get('users/CartView', [CartController::class, 'index'])-> name('users/CartView.index');
Route::post('users/CartView', [CartController::class, 'store'])-> name('users/CartView.store');
Route::put('users/CartView/{id}', [CartController::class, 'update'])-> name('users/CartView.update');
Route::post('users/CartView/remove/{product_id}', [CartController::class, 'remove'])->name('users/CartView.remove');

Route::post('users/ListProductView/unsetSession', [ProductController::class, 'index'])-> name('users/ListProductView.unsetSession');
Route::get('users/CheckoutView', [CheckoutViewController::class, 'index'])-> name('users/CheckoutView.index');
Route::post('users/CheckoutView', [CheckoutViewController::class, 'store'])-> name('users/CheckoutView.store');


Route::get('/public', [publicController::class, 'index'])-> name('public.index');

Route::get('/login', [LoginController::class, 'showForm'])-> name('login');
Route::post('/login', [LoginController::class, 'handleForm'])-> name('handle-login');

Route::get('/register', [RegisterController::class, 'showRegistertrationForm'])-> name('register');
Route::post('/register', [RegisterController::class, 'showRegistertrationForm'])-> name('handle-register');

Route::post('/logout', [LoginController::class, 'logout'])-> name('logout');

Route::get('/users', [usersController::class, 'index'])-> name('users.home');
Route::get('/admin', [adminController::class, 'index'])-> name('admin.home');